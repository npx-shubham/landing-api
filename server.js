const { config } = require("./config/config");
const express = require("express");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const cors = require("cors");
const compression = require("compression");
const mongoose = require("mongoose");
const routes = require('./routes');
const redisClient = require('redis').createClient();

const app = express();

app.use(helmet());
app.use(compression());
app.use(cors());
app.use(bodyParser.json());

const limiter = require('express-limiter')(app, redisClient);
app.use(limiter({
    lookup: ['connection.remoteAddress'],
    total: config.req_rate_limit || 5,
    expire: 1000 * 60 * 60
}));

app.use('/api', routes);

mongoose.connect(config.db_connection_string, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("DB Connected");
    app.listen(config.port, () => {
        console.log(`Server is up and running on ${config.port}`);
    })
}).catch(error => {
    console.log(error);
});
const express = require("express");
const router = express.Router();
const newsletterRouter = require("./controllers/Subscription/routes");
const feedbackRouter = require("./controllers/Feedback/routes");
router.use("/newsletter", newsletterRouter);
router.use("/feedback", feedbackRouter);
module.exports = router;

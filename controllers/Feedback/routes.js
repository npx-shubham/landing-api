const express = require("express");
const { checkSchema } = require("express-validator");
const router = express.Router();
const {
  validationHandler,
  validation,
} = require("../../middlewares/validator");
const { feedback } = require("./Feedback");

router.post(
  "/",
  checkSchema(validation.feedback),
  validationHandler(),
  feedback
);

module.exports = router;

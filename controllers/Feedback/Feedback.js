const Feedback = require("../../models/Feedback");

module.exports = {
  async feedback(req, res) {
    const { email, name, message } = req.body;
    const isAvailable = await Feedback.findOne({ email });
    if (!isAvailable) {
      const feedback = new Feedback({
        email,
        name,
        message,
      });
      try {
        await feedback.save();
        res.status(201).json({
          msg: "Thanks for your feedback, we will get back to you soon.",
        });
      } catch (error) {
        res.status(500).json({
          msg: "Something went wrong, please try after some time.",
        });
      }
    } else {
      res.status(500).json({
        msg:
          "We have already registered your feedback and we are working on it, to contact with us please check our contact-us section.",
      });
    }
  },
};

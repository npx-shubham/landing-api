const Newsletter = require("../../models/Newsletter");

module.exports = {
  async subscribe(req, res) {
    const { email } = req.body;
    const isAvailable = await Newsletter.findOne({ email });
    if (!isAvailable) {
      const newsletter = new Newsletter({
        email,
      });
      try {
        await newsletter.save();
        res.status(201).json({
          msg:
            "Thanks for subscribing our newsletter, you have been successfully added to our subscribers list.",
        });
      } catch (error) {
        res.status(500).json({
          msg: "Something went wrong, please try after some time.",
        });
      }
    } else {
      res.status(500).json({
        msg: "This email is already available in our list.",
      });
    }
  },
  async unsubscribe(req, res) {
    const { email } = req.body;
    const isAvailable = await Newsletter.findOne({ email });
    if (isAvailable) {
      try {
        await Newsletter.deleteOne({ email });
        res.status(200).json({
          msg:
            "You have been successfully removed from this subscriber list and won't receive any further email.",
        });
      } catch (error) {
        res.status(500).json({
          msg: "Something went wrong, please try after some time.",
        });
      }
    } else {
      res.status(500).json({
        msg: "This email does not exist in our system.",
      });
    }
  },
};

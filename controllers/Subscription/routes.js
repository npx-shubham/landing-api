const express = require("express");
const { checkSchema } = require("express-validator");
const router = express.Router();
const {
  validationHandler,
  validation,
} = require("../../middlewares/validator");
const { subscribe, unsubscribe } = require("./NewsletterSubscription");

router.post(
  "/",
  checkSchema(validation.subscription),
  validationHandler(),
  subscribe
);

router.delete(
  "/",
  checkSchema(validation.subscription),
  validationHandler(),
  unsubscribe
);

module.exports = router;


require("dotenv").config();

module.exports = {
    config: {
        port: process.env.PORT || 6000,
        db_connection_string: process.env.DB_URI || 'mongodb://localhost:27017/newsletters',
        req_rate_limit: process.env.RATE_LIMITER_LIMIT || 5
    }
}
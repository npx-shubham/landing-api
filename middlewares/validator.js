const { validationResult } = require("express-validator");

module.exports = {
  validation: Object.freeze({
    feedback: {
      name: {
        exists: true,
        isLength: {
          errorMessage:
            "Name is required field and minimum 3 characters are required.",
          options: { min: 3 },
        },
      },
      email: {
        exists: true,
        isLength: {
          errorMessage:
            "Email is required field and minimum 3 characters are required.",
          options: { min: 3 },
        },
        custom: {
          options: (email) => {
            return new RegExp(
              "[a-zA-Z0-9_]+.[a-zA-Z0-9_]+@[a-zA-Z0-9]+.[a-z]{1,8}"
            ).test(email);
          },
          errorMessage: "Please enter valid email.",
        },
      },
      message: {
        exists: true,
        isLength: {
          errorMessage:
            "Name is required field and message can be maximum 255 characters.",
          options: { min: 1, max: 255 },
        },
      },
    },
    subscription: {
      email: {
        exists: true,
        isLength: {
          errorMessage:
            "Email is required field and minimum 3 characters are required.",
          options: { min: 3 },
        },
        custom: {
          options: (email) => {
            return new RegExp(
              "[a-zA-Z0-9_]+.[a-zA-Z0-9_]+@[a-zA-Z0-9]+.[a-z]{1,8}"
            ).test(email);
          },
          errorMessage: "Please enter valid email.",
        },
      },
    },
  }),
  validationHandler: () => {
    return (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log("Inside Validation Handler", errors);
        res.status(422).json({
          msg: "Validation error",
          errors: errors.errors,
        });
        return;
      }
      next();
    };
  },
};

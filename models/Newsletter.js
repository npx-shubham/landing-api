const mongoose = require("mongoose");

const NewsletterSchema = mongoose.Schema({
    email: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model("Newsletter", NewsletterSchema);